package sqlite

import (
	"fmt"

	"bitbucket.org/TAI_wutipong/test_db_select/address"
)

type AddressRepository struct {
}

type AddressRepositoryFactory struct {
}

type AddressRepositoryContext struct {
	index int
}

func (AddressRepositoryFactory) Make(param string) (repo address.AddressRepository, context interface{}) {
	var a AddressRepository
	var c AddressRepositoryContext

	c.index = 200

	repo = a
	context = c

	return
}

func (AddressRepository) PrintHomeAddress(context interface{}) {
	fmt.Println(`D. Richard Hipp
	Hwaci - Applied Software Research
	704.948.4565
	drh@hwaci.com`)
}

func init() {
	var f AddressRepositoryFactory
	address.RegisterFactory("sqlite", f)
}
