package main

import (
	"bitbucket.org/TAI_wutipong/test_db_select/address"
	_ "bitbucket.org/TAI_wutipong/test_db_select/address/oracle"
	_ "bitbucket.org/TAI_wutipong/test_db_select/address/sqlite"
)

func main() {
	s, c := address.Open("sqlite", "data.sqlite")
	s.PrintHomeAddress(c)

	o, co := address.Open("oracle", "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCPS)(HOST=mib.com)(PORT=7777))(CONNECT_DATA=(SERVICE_NAME=something)))")
	o.PrintHomeAddress(co)
}
