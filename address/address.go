package address

type AddressRepository interface {
	PrintHomeAddress(context interface{})
}

type AddressRepositoryFactory interface {
	Make(param string) (AddressRepository, interface{})
}

var factoryMap = make(map[string]AddressRepositoryFactory)

func RegisterFactory(name string, f AddressRepositoryFactory) {
	factoryMap[name] = f
}

func Open(name string, param string) (AddressRepository, interface{}) {
	return factoryMap[name].Make(param)
}
