package oracle

import (
	"fmt"

	"bitbucket.org/TAI_wutipong/test_db_select/address"
)

type AddressRepository struct {
	SomeParam int
}

type AddressRepositoryFactory struct {
}

type AddressRepositoryContext struct {
	param string
}

func (AddressRepositoryFactory) Make(param string) (repo address.AddressRepository, context interface{}) {
	var a AddressRepository
	var c AddressRepositoryContext

	c.param = param

	repo = a
	context = c

	return
}

func (AddressRepository) PrintHomeAddress(context interface{}) {
	fmt.Println(`Oracle Corporation
	500 Oracle Parkway
	Redwood Shores, CA 94065`)
}

func init() {
	var f AddressRepositoryFactory
	address.RegisterFactory("oracle", f)
}
